mkdir -p /srv/preware/build/nizovn_apps
cp -Rf packages/* /srv/preware/build/nizovn_apps
mkdir -p ipkgs
for dir in /srv/preware/build/nizovn_apps/*/
do
	if [ -f $dir/Makefile ]; then
		make -C $dir
		cp $dir/ipkgs/*.ipk ipkgs/
	fi
done
cp -l IpkgFeedGenerator.jar ipkgs/
cd ipkgs
java -jar IpkgFeedGenerator.jar

awk '{$1=$1}1' RS='\n\n\n' FS='\n' OFS='\1' ORS='\n' Packages \
  | sort -i \
  | awk '{$1=$1}1' FS='\1' OFS='\n' ORS='\n\n\n' > Packages.tmp
mv Packages.tmp Packages
gzip -c Packages > Packages.gz
