NAME     = squid
TITLE    = Squid SSL Bump
APP_ID   = com.nizovn.${NAME}
SRC_VER  = 4.8
VERSION  = 4.8-0
TYPE     = Application
CATEGORY = System Utilities
HOMEPAGE = http://www.squid-cache.org
MAINTAINER = nizovn <nizovn@gmail.com>
ICON     = https://gitlab.com/nizovn/com.nizovn.squid/raw/master/icon.png
MINWEBOSVERSION = 1.3.5
DESCRIPTION = Squid Web Proxy allows bumping insecure SSL connections when required in a sense providing TLS 1.2 support to webOS.<br>\
This is done transparently by intercepting webOS connections, decrypting them, and then encrypting using higher protocol.\
Squid certificate should be installed in webOS certificate manager to get rid of validation errors.<br>\
The proxy forwarding must be enabled by any suitable webOS proxy application, e.g. ProxySwitch.<br>
CHANGELOG = \
<table> \
<tr><td valign="top">4.8-0:</td> \
<td>Initial version</td></tr> \
</table>

SCREENSHOTS = [\
\"https://gitlab.com/nizovn/com.nizovn.squid/raw/2c980d0a317d6f6a1568f0eb14186f0e3eec0183/screenshots/squid_2019-08-09_160519.png\"\
]
LICENSE  = GPL v2 Open Source

DEPENDS  = com.nizovn.glibc, com.nizovn.openssl
DEVICECOMPATIBILITY = [\"Pre3\",\"TouchPad\",\"Touchpad Go\"]

SRC_TGZ = http://www.squid-cache.org/Versions/v4/squid-${SRC_VER}.tar.gz

ARCH_LIST = armv7

.PHONY: package
package: $(foreach arch,${ARCH_LIST},ipkgs/${APP_ID}_${VERSION}_${arch}.ipk)
include ../../support/package.mk

include ../../support/download.mk

.PHONY: unpack
unpack: build/.unpacked-${VERSION}

build/.unpacked-app-${VERSION}:
	rm -rf build/src-app
	mkdir -p build
	cp -rf /home/nizovn/webos/qt5/qt5.9/git/com.nizovn.squid build/src-app
	sed -i.orig \
	  -e 's/"title": ".*"/"title": "${TITLE}"/g' \
	  -e 's/"id": ".*"/"id": "${APP_ID}"/g' \
	  -e 's/"version": ".*"/"version": "${VERSION}"/g' \
	  -e 's/"release_date": ".*"/"release_date": "'"`date -I`"'"/g' \
	  build/src-app/appinfo.json
	rm -f build/src-app/appinfo.json.orig
	touch $@

build/.unpacked-${VERSION}: ${DL_DIR}/${NAME}-${SRC_VER}.tar.gz build/.unpacked-app-${VERSION}
	rm -rf build/src
	tar -C build -xf ${DL_DIR}/${NAME}-${SRC_VER}.tar.gz
	mv build/${NAME}-${SRC_VER} build/src
	patch -d build/src -p0 < squid-skip-filedescriptors-test.patch
	( cd build/src ; sh bootstrap.sh )
	touch $@

.PHONY: build
build: build/.built-${VERSION}

build/.built-${VERSION}: $(foreach arch,${ARCH_LIST},build/${arch}.built-${VERSION}) $(foreach arch,${ARCH_LIST},build/${arch}.built-app-${VERSION})
	touch $@

include ../../support/cross-compile.mk
# qt5-webos-sdk
QT5_WEBOS_PATH = /opt/qt5-webos-sdk
CROSS_COMPILE_armv7 = ${QT5_WEBOS_PATH}/gcc-linaro-4.8-2015.06-x86_64_arm-linux-gnueabi/bin/arm-linux-gnueabi-
SYSROOT = ${QT5_WEBOS_PATH}/gcc-linaro-4.8-2015.06-x86_64_arm-linux-gnueabi/arm-linux-gnueabi/libc
GLIBC_RPATH = /media/cryptofs/apps/usr/palm/applications/com.nizovn.glibc/lib
OPENSSL_RPATH = /media/cryptofs/apps/usr/palm/applications/com.nizovn.openssl/lib

CONFIGURE_OPTIONS = \
	--with-openssl --without-gnutls --without-libxml2 \
	--without-mit-krb5 --without-heimdal-krb5

CFLAGS = --sysroot=${SYSROOT} -O2 -march=armv7-a -mtune=cortex-a8 -mfpu=neon -mfloat-abi=softfp -ftree-vectorize \
	-I${QT5_WEBOS_PATH}/device/include
LDFLAGS = -Wl,--dynamic-linker=${GLIBC_RPATH}/ld.so \
	-Wl,-rpath,${GLIBC_RPATH} -Wl,-rpath,${OPENSSL_RPATH} \
	-L${QT5_WEBOS_PATH}/device/lib

BUILD_COMMAND_armv7 = \
	CC=${CROSS_COMPILE_armv7}gcc \
	CXX=${CROSS_COMPILE_armv7}g++ \
	CFLAGS="${CFLAGS}" \
	CXXFLAGS="${CFLAGS}" \
	LDFLAGS="${LDFLAGS}" \
	./configure --build=i686-pc-linux-gnu --host=arm-linux-gnueabi \
	  --prefix=/media/cryptofs/apps/usr/palm/applications/${APP_ID}/squid \
	  ${CONFIGURE_OPTIONS} ; \
	${MAKE} -C src cf_gen ; \
	cp ../cf_gen src/ ; \
	${MAKE} \
		AR=${CROSS_COMPILE_armv7}ar RANLIB=${CROSS_COMPILE_armv7}ranlib

build/cf_gen: build/.unpacked-${VERSION}
	( cd build/src ; \
	make distclean ; \
	./configure \
	  ${CONFIGURE_OPTIONS} ; \
	  cd src ; make cf_gen ; \
	cp cf_gen ../.. ; \
	make distclean )

build/%.built-app-${VERSION}: build/.unpacked-${VERSION}
	( cd build/src-app/c-service ; \
	  ${MAKE} DEVICE=1 VERSION=${VERSION} STAGING_DIR=${STAGING_DIR_$*} CC=${CROSS_COMPILE_$*}gcc CFLAGS=${CFLAGS_$*} \
	  clobber squid_service \
	)
	mkdir -p build/$*/usr/palm/applications/${APP_ID}
	( cd build/src-app ; \
	  rsync -av --progress ./ ../$*/usr/palm/applications/${APP_ID}/ --exclude-from=.gitignore --exclude=c-service --exclude=Makefile --exclude=.git --exclude=.gitignore \
	)
	mkdir build/$*/usr/palm/applications/${APP_ID}/c-service
	cp build/src-app/c-service/squid_service build/$*/usr/palm/applications/${APP_ID}/c-service/
	cp build/src-app/c-service/com* build/$*/usr/palm/applications/${APP_ID}/c-service/
	touch $@

build/armv7.built-${VERSION}: build/cf_gen
	rm -rf build/armv7
	( cd build/src ; \
	  ${BUILD_COMMAND_armv7} )
	mkdir -p build/armv7/
	( cd build/src ; \
	  ${MAKE} install DESTDIR=`pwd`/../armv7/ )
	mv build/armv7/media/cryptofs/apps/* build/armv7/
	rm -rf build/armv7/media
	find build/armv7/usr/palm/applications/${APP_ID}/squid/share/errors/ -type l -delete
	cp build/armv7/usr/palm/applications/${APP_ID}/squid/etc/squid.conf.default build/armv7/usr/palm/applications/${APP_ID}/squid/etc/squid.conf
	sed -i 's:http_port 3128:http_port 3128 ssl-bump cert=/media/cryptofs/apps/usr/palm/applications/com.nizovn.squid/squid/etc/squid.pem\
	visible_hostname squid\
	acl step1 at_step SslBump1\
	ssl_bump peek step1\
	ssl_bump bump all\
	shutdown_lifetime 1 seconds\
	cache deny all\
	access_log none\
	cache_store_log none\
	cache_log /dev/null\
	cache_mem 500 KB\
	sslproxy_session_cache_size 500 KB\
	:g' build/armv7/usr/palm/applications/${APP_ID}/squid/etc/squid.conf

	touch $@

build/%/CONTROL/postinst:
	mkdir -p build/$*/CONTROL
	install -m 0775 build/src-app/control/postinst $@
	chmod ugo+x $@

build/%/CONTROL/prerm:
	mkdir -p build/$*/CONTROL
	install -m 0775 build/src-app/control/prerm $@
	chmod ugo+x $@

clobber::
	rm -rf build
