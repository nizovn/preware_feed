Packaging is based on webOS-Internals Preware Makefiles (https://github.com/webos-internals/build).

## Credits
IpkgFeedGenerator.jar: https://code.google.com/archive/p/ipkg-feed-generator/downloads

## How to install
Add a feed in Preware using url: https://gitlab.com/nizovn/preware_feed/raw/master/ipkgs/
